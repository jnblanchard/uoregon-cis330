
#include "maze.hpp"

#include <iostream>
#include <string>

Maze::Direction newHeading[4][4] = {
    {Maze::LEFT, Maze::DOWN, Maze::RIGHT, Maze::UP},      // DOWN
    {Maze::DOWN, Maze::RIGHT, Maze::UP, Maze::LEFT},     // RIGHT
    {Maze::RIGHT, Maze::UP, Maze::LEFT, Maze::DOWN},     // UP
    {Maze::UP, Maze::LEFT, Maze::DOWN, Maze::RIGHT}     // LEFT
};

Position headingChange[4] = {
    {1, 0},         // DOWN
    {0, 1},        // RIGHT
    {-1, 0},       // UP
    {0, -1}       // LEFT
};

const char *headingName[] = {
    "DOWN",
    "RIGHT",
    "UP",
    "LEFT"
};

Maze::Maze(int size)
{
    this->size = size;
    allocateMazeSize();
}


void Maze::readFromFile(std::ifstream &f)
{
    std::streampos cPos = f.tellg();
	std::string theline;
	getline(f, theline);
	for (int i = 0; i < size; i++) {
		getline( f, theline );
        for (int j = 0; j < size; j++) {
            this->maze[i][j] = theline[j];
        }
	}
	// reset the file stream
	f.seekg(cPos);
    
    findEntrance();
    
}

// make a single step advancing toward the exit
void Maze::step()
{
    Maze::Direction *preferredDirection = newHeading[this->heading];
    for (int i = 0; i < 4; i++) {
        heading = preferredDirection[i];
        Position delta = headingChange[heading];
        if (maze[pos.row + delta.row][pos.col + delta.col] != '@') {
            this->pos.row += delta.row;
            this->pos.col += delta.col;
            break;
        }
    }
   
}

// return true if the maze exit has been reached, false otherwise
bool Maze::atExit()
{
    return pos.row == 0 || pos.row == (size - 1) || pos.col == 0 || pos.col == (size -1);
}

// set row and col to current position of 'x'
void Maze::getCurrentPosition(int &row, int &col)
{
    row = pos.row;
    col = pos.col;
}

const char *Maze::getHeading()
{
    return headingName[heading];
}

void Maze::allocateMazeSize() {
    char ** _maze;
    _maze = (char **) malloc(size * sizeof(char*));
    for(int i = 0; i < size; i++) {
        _maze[i] = (char *) malloc(size * sizeof(char));
        for (int j = 0; j < size; j++) {
            _maze[i][j] = '.';
        }
    }
    this->maze = _maze;
}

void Maze::findEntrance()
{
    bool foundEntry = false;
    Position entrance = {.row = size-1, .col = 0};
    
    for (entrance.col = 0; !foundEntry && entrance.col < size - 1; entrance.col++) {
        if (maze[entrance.row][entrance.col] == 'x') {
            foundEntry = true;
            break;
        }
    }
    
    if (!foundEntry) {
        entrance.row = entrance.col = 0;
    }
    
    this->pos.row = entrance.row;
    this->pos.col = entrance.col;
    this->heading = UP;
}
