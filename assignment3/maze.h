#ifndef __MAZE_H_
#define __MAZE_H_

typedef enum{ North = 0, South, West, East} Heading;

typedef struct{
    int row;
    int col;
} Position;

extern const char deltaByHeading[4];

extern const newHeading[4][4];

void solveMaze(Heading heading, Position pos, char ** maze, int mazeSize, Position delta);

void printMaze(int mazeSize, char** maze);

void createMaze(int size, char** maze, char* str, int row);

void allocateMazeSize(int size,char *** maze)


#endif