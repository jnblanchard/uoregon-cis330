#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "maze.h"

Position deltaByHeading[4] = {
    {-1,0}, {1,0}, {0,-1}, {0,1}
};

Heading newHeading[4][4] = {
    {East, North, West, South}, {West, South, East, North}, {North, West, South, East}, {South, East, North, West}
};

int main()
{
    FILE *ip;
    char* path = "/Users/sblancha/desktop/cs330/input.txt";
    char* str;
    int numGraphs;
    int mazeSize;
    char buffer[100];
    char** maze;
    int indexY;
    int bufferSize = sizeof(buffer) / sizeof(buffer[0]);
    
    ip = fopen( path, "r");
    
    if (ip == NULL) {
        printf("error while opening the file. \n");
    }
    str = fgets(buffer, bufferSize, ip);
    numGraphs = atoi(str);
    for(int j = 0; j < numGraphs; j++) {
        str = fgets(buffer, bufferSize, ip);
        mazeSize = atoi(str);
        allocateMazeSize(mazeSize, &maze);
        for(int i = 0; i < mazeSize; i++) {
            str = fgets(buffer, bufferSize, ip);
            createMaze(mazeSize, maze, str, i);
        }
        for(int i = 0; i < mazeSize; i++) {
            if(maze[mazeSize-1][i] == 'x') {
                printf("%s", "ENTER\n");
                indexY = i;
            }
            
        }
        Position pos = { mazeSize-1, indexY };
        Heading heading = 0;
        Position delta = {0,0};
        solveMaze(heading, pos, maze, mazeSize, delta);
        printf("%s", "EXIT\n");
        printf("%s", "***\n");
    }
    fclose(ip);
}
