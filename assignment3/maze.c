#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>




typedef enum{ North = 0, South, West, East} Heading;

typedef struct{
    int row;
    int col;
} Position;

Position deltaByHeading[4] = {
    {-1,0}, {1,0}, {0,-1}, {0,1}
};

Heading newHeading[4][4] = {
    {East, North, West, South}, {West, South, East, North}, {North, West, South, East}, {South, East, North, West}
};

void allocateMazeSize(int size,char *** maze) {
    char ** _maze;
    _maze = malloc(size * sizeof(char*));
    int i = 0;
    for( i; i < size; i++) {
        _maze[i] = malloc(size * sizeof(char));
    }
    *maze = _maze;
}


void createMaze(int size, char** maze, char* str, int row) {
	int i = 0;
    for(i; i < size; i++) {
        maze[row][i] = str[i];
    }
}

void printMaze(int mazeSize, char** maze) {
	int i = 0;
	int j = 0;
    for( i ; i < mazeSize; i++) {
        for( j ; j < mazeSize; j++){
            printf("%c", maze[i][j]);
        }
        printf("\n");
    }
}

void solveMaze(Heading heading, Position pos, char ** maze, int mazeSize, Position delta ) {
    do {
    	int i = 0;
        Heading *preferences = newHeading[heading];
        for(i; i < 4; i++) {
            Heading possibleHeading = preferences[i];
            Position delta = deltaByHeading[possibleHeading];
            if((pos.row+delta.row) == 0 || (pos.col+delta.col) == 0) {
                Position _pos = { pos.row + delta.row , pos.col + delta.col};
                pos = _pos;
                return;
            }
            if(maze[pos.row+delta.row][pos.col+delta.col] == '@' || maze[pos.row+delta.row][pos.col+delta.col] != '.') {
                
            }
            else {
                Position _pos = { pos.row + delta.row , pos.col + delta.col};
                pos = _pos;
                heading = possibleHeading;
                if(delta.row == -1) {
                    printf("%s", "UP\n");
                }
                if(delta.row == 1) {
                    printf("%s", "DOWN\n");
                }
                if(delta.col == 1) {
                    printf("%s", "RIGHT\n");
                }
                if(delta.col == -1) {
                    printf("%s", "LEFT\n");
                }
                i = 4;
            }
        }
    } while ( pos.row != (mazeSize-1) || pos.col != 0 || pos.col != (mazeSize -1));
}





int main(int argc, const char * argv[])
{
    if( argc != 2) {
        printf("expecting one argument");
        exit(1);
    }
    FILE *ip;
    const char* path = argv[1];
    char* str;
    int numGraphs;
    int mazeSize;
    char buffer[100];
    char** maze;
    int indexY;
    int j = 0;
    int i = 0;
    int bufferSize = sizeof(buffer) / sizeof(buffer[0]);
    
    ip = fopen( path, "r");
    
    if (ip == NULL) {
        printf("error while opening the file. \n");
    }
    str = fgets(buffer, bufferSize, ip);
    numGraphs = atoi(str);
    for(j = 0; j < numGraphs; j++) {
        str = fgets(buffer, bufferSize, ip);
        mazeSize = atoi(str);
        allocateMazeSize(mazeSize, &maze);
        for(i = 0; i < mazeSize; i++) {
            str = fgets(buffer, bufferSize, ip);
            createMaze(mazeSize, maze, str, i);
        }
        for(i = 0; i < mazeSize; i++) {
            if(maze[mazeSize-1][i] == 'x') {
                printf("%s", "ENTER\n");
                indexY = i;
            }
            
        }
        Position pos = { mazeSize-1, indexY };
        Heading heading = 0;
        Position delta = {0,0};
        solveMaze(heading, pos, maze, mazeSize, delta);
        printf("%s", "EXIT\n");
        printf("%s", "***\n");
    }
    fclose(ip);
}
