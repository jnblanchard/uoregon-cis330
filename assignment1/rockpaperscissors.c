#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>



char* getUserChoice() {
    /* Prompt the user "Enter rock, paper, or scissors: " and return
     the string they enter */
	static char userChoice[20];
	printf("Enter rock, paper, or scissors. \n");
	scanf("%s", userChoice);
	return userChoice;
}

char* getComputerChoice() {
    srand (time(NULL));
    /* get a pseudo-random integer between 0 and 2 (inclusive) */
    /* If randChoice is 0, return "rock", otherwise if randChoice is 1,
     return "paper", and if randChoice is 2, return "scissors". */
    int randChoice = rand() % 3;
    
    if (randChoice == 0) {
        return "rock";
    }
    
    if (randChoice == 1) {
        return "paper";
    }
    
    if (randChoice == 2) {
        return "scissors";
    }
    return 0;
    
}

int comparsionInteger(char* choice) {
    if (!strcmp(choice, "rock")) {
        return 0;
    }
    if (!strcmp(choice, "paper")) {
        return 1;
    }
    if (!strcmp(choice, "scissors")) {
        return 2;
    }
    return 0;
}

char* compare(char* choice1, char* choice2)
{
    /* Implement the logic of the game here. If choice1 and choice2
     are equal, the result should be "This game is a tie."
     
     Make sure to use strcmp for string comparison.
     */
    char table[3][3] = {{'T', 'L', 'W'}, {'W', 'T', 'L'}, {'L', 'W', 'T'}};
    int int1;
    int int2;
    int1 = comparsionInteger(choice1);
    int2 = comparsionInteger(choice2);
    
    char outcome = table[int1][int2];
    if (outcome == 'T') {
        return "This game is a tie";
    }
    if (outcome == 'L') {
        return "This game the Computer Wins";
    }
    if (outcome == 'W') {
        return "This game the User Wins";
    }
    return 0;
    
    
    
    

    return 0;
}

void play() {
    char *userChoice, *computerChoice, *outcome;
    
    userChoice = getUserChoice();
    computerChoice = getComputerChoice();
    
    outcome = compare(userChoice, computerChoice);
    
    printf("You picked %s.\n", userChoice);
    printf("Computer picked %s.\n", computerChoice);
    printf("%s\n", outcome);
}

int playAgainPrompt() {
    static char cont[20];
    printf("Play Again? \n");
    scanf("%s" , cont);
    if(!strcmp(cont, "YES") || !strcmp(cont, "yes") || !strcmp(cont, "y") || !strcmp(cont, "Y")) {
        return 1;
    }
    else {
        return 0;
    }
}

int main(int argc, char** argv)
{

    play();
    int playAgain = playAgainPrompt();

    
    while (playAgain != 0) {
        play();
        playAgain = playAgainPrompt();
    }
    return 0;
}