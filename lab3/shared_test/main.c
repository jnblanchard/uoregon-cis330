#include <stdio.h>
#include <math.h>
#include "power.h"

int main(int argc, char* argv[]) {
	double a, b, c;
	a = 3.5342;
	b = 4.3214;
	c = power(a, b);
	
	printf("%f\n", c);
	
	return 0;
}