#include <stdio.h>
#include "square1.h"
#include "square2.h"
#include "diamond.h"

int main(int argc, const char * argv[]) {
	print10Square();
	printf("\n");
	int input = prompt();
	printf("\n");
    char **square;
    allocateNumberSquare(input, &square);
    initalizeNumberSquare(input, square);
    printNumberSquare(input, square);
    printf("\n");
    printNumberDiamond(input, square);
    deallocateNumberSquare(input, square);
}