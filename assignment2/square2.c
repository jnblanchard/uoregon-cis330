#include "square2.h"

int inputNum() {
    int input;
    printf("\n Please enter the size of the square [2-10]:");
    scanf("%d", &input);
    return input;
}

void allocateNumberSquare(const int size, char ***square) {
    char ** _square;
    _square = malloc(size * sizeof(char *));
    for (int i = 0; i < size; i++) {
        _square[i] = malloc(size * sizeof(char));
    }
    *square = _square;
}

void initalizeNumberSquare(const int size, char **square) {
    for(int i = 0;  i < size; i++) {
        for(int j = 0; j < size; j++) {
            square[i][j] = j;
        }
    }
}

void printNumberSquare(const int size, char **square) {
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++) {
            printf("%d ", square[i][j]);
        }
         printf("\n");
    }
     printf("\n");
}

void deallocateNumberSquare(const int size, char **square) {
    for(int i = 0; i < size; i++) {
        free(square[i]);
    }
    if (square) {
        free(square);
    }
}

int prompt() {
    int input = 0;
    
    do {
        input = inputNum();
    }
    while(input < 2 || input > 10);
    return input;
}