#include "square1.h"

void print10Square() {
	static char square[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    
    for (int i=0; i<10; i++) {
        for(int j = 0; j<10; j++) {
            printf("%d ", square[j]);
        }
         printf("\n");
    }
    printf("\n");
}