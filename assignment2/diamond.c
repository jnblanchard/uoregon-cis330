#include "diamond.h"

void printNumberDiamond(const int size, char **square) {
	int numDigits = 1, numSpaces = 0, calcMiddle = (size/2) + 1;
    for(int i = 1; i <= size; i++) {
        numSpaces = size - numDigits;
        int xSpaces = numSpaces/2;
        printf("\n");
        for(int x = 1; x <= xSpaces; x++) {
            printf("  ");
        }
        for(int y = 1; y <= numDigits; y++) {
            printf("%d", square[i-1][y-1]);
            printf(" ");
        }
        for(int p = 1; p <= xSpaces; p++) {
            printf("  ");
        }
        if(i >= calcMiddle) {
            numDigits = numDigits - 2;
        } else {
            numDigits = numDigits + 2;
        }
        if(numDigits > size) {
            numDigits = size - 1;
        }
    }
     printf("\n");
}