/*
 * maze.hpp
 *
 *  Created on: Jan 27, 2014
 *      Author: norris
 */

#ifndef MAZE_HPP_
#define MAZE_HPP_

#include <fstream>

typedef struct {
    int row;
    int col;
} Position;

class Maze
{
public:
	Maze(int size);
	~Maze() {}

	enum Direction { DOWN, RIGHT, UP, LEFT };

	// Implement the following functions:

	// read maze from file, find starting location
	void readFromFile(std::ifstream &f);

	// make a single step advancing toward the exit
	void step();

	// return true if the maze exit has been reached, false otherwise
	bool atExit();

	// set row and col to current position of 'x'
	void getCurrentPosition(int &row, int &col);

    const char *getHeading();
    
	// You can add more functions if you like
private:
	// Private data and methods
    
    void allocateMazeSize();
    void findEntrance();
    
    int size;
    char **maze;
    Direction heading;
    Position pos;
    
};


#endif /* MAZE_HPP_ */
