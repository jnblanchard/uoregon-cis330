//
//  CaesarCipher.h
//  Encryption
//
//  Created by John Blanchard on 2/6/14.
//  Copyright (c) 2014 John Blanchard. All rights reserved.
//

#ifndef CAESARCIPHER_HPP_
#define CAESARCIPHER_HPP_

#include "cipher.hpp"

class CaesarCipher : public Cipher {
public:
	CaesarCipher();
	virtual ~CaesarCipher();
	virtual std::string encrypt( std::string &text );
	virtual std::string decrypt( std::string &text );
private:
	int rotation;
};

#endif 
