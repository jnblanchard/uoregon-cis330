//
//  DateCipher.h
//  Encryption
//
//  Created by John Blanchard on 2/6/14.
//  Copyright (c) 2014 John Blanchard. All rights reserved.
//

#ifndef DATECIPHER_HPP_
#define DATECIPHER_HPP_

#include "cipher.hpp"

class DateCipher : public Cipher {
public:
	DateCipher();
	virtual ~DateCipher();
	virtual std::string encrypt( std::string &text );
	virtual std::string decrypt( std::string &text );
private:
	int rotation;
};

#endif
