//
//  CaesarCipher.cpp
//  Encryption
//
//  Created by John Blanchard on 2/6/14.
//  Copyright (c) 2014 John Blanchard. All rights reserved.
//

#include "cipher.hpp"
#include "CaesarCipher.hpp"

 static int const key = 5;

// Single-argument constructor
CaesarCipher::CaesarCipher() : Cipher(), rotation(13) {
	// Nothing else to do in the constructor
}

// Destructor
CaesarCipher::~CaesarCipher() {
}

// Overloaded encrypt method
std::string
CaesarCipher::encrypt( std::string &inputText ) {
	std::string text = inputText;
	std::string::size_type len = text.length();
	for (int i = 0; i != len; ++i) {
        int temp;
        int ch = (int)text[i];
        int x;
        if(ch == 32) {
            ch = 'z' + 1;
            x = 32;
        }
        if(islower(ch)) {
            temp = ch + key - 'z';
            ch = ch + key;
            if(ch > 'z') {
                ch = 'a' - 1 + temp;
            }
        }
        else if (isupper(ch)) {
            temp = ch + key - 'Z';
            ch = ch + key;
            if(ch > 'Z') {
                ch = 'A' - 1 + temp;
            }
        }
        if(ch == 'z' + 1) {
            ch = x;
        }
        text[i] = (char)ch;
    }
	return text;
}

std::string
CaesarCipher::decrypt( std::string &text ) {
	std::string intext = text;
	std::string::size_type len = text.length();
	for (int i = 0; i != len; ++i) {
        int ch = (int)intext[i];
        int temp;
        if(islower(ch)) {
            ch = ch - key;
            temp = 'a' - ch;
            if(ch < 'a') {
                ch = 'z' + 1 - temp;
            }
        }
        else if (isupper(ch)) {
            ch = ch - key;
            temp = 'A' - ch;
            if(ch < 'A') {
                ch = 'Z' + 1 - temp;
            }
        }
        intext[i] = (char)ch;
    }
	return intext;
}
